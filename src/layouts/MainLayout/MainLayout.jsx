import React, { Component, PropTypes } from 'react';
import { Router, Route, IndexRoute, Link } from 'react-router';

import ResponsiveNavHeader from '../../components/ResponsiveNavHeader'
import styles from './MainLayout.less';

const MainLayout = ({ children }) => {
  return (
    <div>
      <ResponsiveNavHeader />
        <div>
          {children}
        </div>
    </div>
  );
};

MainLayout.propTypes = {
  children: PropTypes.element.isRequired,
};

export default MainLayout;
