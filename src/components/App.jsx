import React, { Component, PropTypes } from 'react';
import HomePage from './HomePage';
import MainLayout from '../layouts/MainLayout/MainLayout';

const App = ({ location }) => {
  return (
    <MainLayout>
      <HomePage />
    </MainLayout>
  );
};

App.propTypes = {
};

export default App;
