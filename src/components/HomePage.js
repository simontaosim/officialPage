'use strict';

import React from 'react';

import styles from "./HomePage.css"

class HomePage extends React.Component {

  constructor(props) {
      super(props);
  }

  componentDidMount() {
    $(document).ready(function(){
      startOnePage({
        frame: "#view",
        container: "#frame",
        sections: ".op-section",
        radio: "#radio",
        radioOn: "#radioOn",
        speed: 500,
        easing: "swing"
      });
    });
  }

  render() {
    return (
      <div>
    <br/><br/>
      <div id="view">
        <div id="frame">
          <div className="op-section">
            <header className="jq22-header" style={{background: "url('lib/img/lv_trip_bag.jpg') no-repeat", width:"100%", backgroundPosition: "center", backgroundClip: "border-box"}}>
                  <br/><br/><br/><br/><br/><br/>
                  <div className="container-fluid">
                  <h1>我们的最新产品</h1>
                 <br/><br/><br/><br/><br/><br/>
                  <p><a className="btn btn-primary btn-lg" href="#" role="button">了解更多</a></p>
                   </div>
                   <br/><br/><br/><br/><br/><br/>
                   <br/><br/><br/><br/><br/><br/><br/>
            </header>
          </div>
          <div className="op-section second">section1</div>
          <div className="op-section third">section2</div>
          <div className="op-section fourth">section3</div>
          <div className="op-section fifth">section4</div>
                <div className="op-section fifth">section5</div>
        </div>
      </div>
        <div id="radioWrap">
          <ul id="radio">
            <li>section0</li>
            <li>section1</li>
            <li>section2</li>
            <li>section3</li>
            <li>section4</li>
            <li>section5</li>
          </ul>
          <span id="radioOn"></span>
        </div>
      </div>
    );
  }
}

export { HomePage as default };
