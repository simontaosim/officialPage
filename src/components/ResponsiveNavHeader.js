'use strict';

import React from 'react';

import "./ResponsiveNavHeader.less"

class ResponsiveNavHeader extends React.Component {

  constructor(props) {
      super(props);
  }

  componentDidMount() {

  }

  render() {
    return (
      <div className="menu-container">
            <div className="menu">
                <ul>
                    <li><a href="#">首页</a></li>
                    <li><a href="#">产品分类</a>
                        <ul>
                            <li><a href="#">School</a>
                                <ul>
                                    <li><a href="#">Lidership</a></li>
                                    <li><a href="#">History</a></li>
                                    <li><a href="#">Locations</a></li>
                                    <li><a href="#">Careers</a></li>
                                </ul>
                            </li>
                            <li><a href="#">Study</a>
                                <ul>
                                    <li><a href="#">Undergraduate</a></li>
                                    <li><a href="#">Masters</a></li>
                                    <li><a href="#">International</a></li>
                                    <li><a href="#">Online</a></li>
                                </ul>
                            </li>
                            <li><a href="#">Research</a>
                                <ul>
                                    <li><a href="#">Undergraduate research</a></li>
                                    <li><a href="#">Masters research</a></li>
                                    <li><a href="#">Funding</a></li>
                                </ul>
                            </li>
                            <li><a href="#">Something</a>
                                <ul>
                                    <li><a href="#">Sub something</a></li>
                                    <li><a href="#">Sub something</a></li>
                                    <li><a href="#">Sub something</a></li>
                                    <li><a href="#">Sub something</a></li>
                                </ul>
                            </li>
                        </ul>
                    </li>
                    <li><a href="#">主题</a>
                        <ul>
                            <li><a href="#">Today</a></li>
                            <li><a href="#">Calendar</a></li>
                            <li><a href="#">Sport</a></li>
                        </ul>
                    </li>
                    <li><a href="#">联系我们</a>
                        <ul>
                            <li><a href="#">School</a>
                                <ul>
                                    <li><a href="#">Lidership</a></li>
                                    <li><a href="#">History</a></li>
                                    <li><a href="#">Locations</a></li>
                                    <li><a href="#">Careers</a></li>
                                </ul>
                            </li>
                            <li><a href="#">Study</a>
                                <ul>
                                    <li><a href="#">Undergraduate</a></li>
                                    <li><a href="#">Masters</a></li>
                                    <li><a href="#">International</a></li>
                                    <li><a href="#">Online</a></li>
                                </ul>
                            </li>
                            <li><a href="#">Study</a>
                                <ul>
                                    <li><a href="#">Undergraduate</a></li>
                                    <li><a href="#">Masters</a></li>
                                    <li><a href="#">International</a></li>
                                    <li><a href="#">Online</a></li>
                                </ul>
                            </li>
                            <li><a href="#">Empty sub</a></li>
                        </ul>
                    </li>
                </ul>
            </div>
        </div>
    );
  }
}

export { ResponsiveNavHeader as default };
